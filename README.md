# manageData

Market data is a broad term for trade-related data. It encompasses a range of information such as price, bid/ask quotes and market volume for financial instruments. Financial firms, traders and investors require such data to execute trades. Market da